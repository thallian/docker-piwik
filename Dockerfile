FROM quay.io/thallian/php7-fpm:latest

ENV VERSION 3.0.4

ENV FPMUSER nginx
ENV FPMGROUP nginx

ADD /rootfs /

RUN apk add --no-cache \
    mariadb-client \
    nginx \
    libressl \
    git \
    tar \
    geoip \
    php7 \
    php7-pear \
    php7-phar \
    php7-openssl \
    php7-json \
    php7-iconv \
    php7-mbstring \
    php7-zlib \
    php7-pdo \
    php7-pdo_mysql \
    php7-mysqli \
    php7-mcrypt \
    php7-session \
    php7-xml \
    php7-curl \
    php7-gd \
    php7-dom \
    php7-ctype \
    php7-apcu \
    php7-dev \
    make \
    g++ \
    autoconf \
    geoip-dev

RUN sed -i "$ s|\-n||g" /usr/bin/pecl
RUN pecl install geoip-1.1.1

RUN addgroup -g 2222  access
RUN addgroup nginx access

RUN ln -s /usr/bin/php7 /usr/bin/php

RUN mkdir /var/lib/piwik
RUN wget -qO- https://builds.piwik.org/piwik-$VERSION.tar.gz | tar -xz -C /var/lib/piwik --strip 1

RUN wget -qO- http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz | gunzip > /usr/share/GeoIP/GeoIPCity.dat

ENV HOME /var/lib/piwik
WORKDIR /var/lib/piwik

RUN wget -O composer-setup.php https://getcomposer.org/installer
RUN php7 composer-setup.php
RUN rm composer-setup.php

RUN php7 composer.phar install

RUN chown -R nginx:nginx /var/lib/piwik

RUN mkdir /run/nginx

RUN apk del php7-dev make g++ autoconf geoip-dev

EXPOSE 80
