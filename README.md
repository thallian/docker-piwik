[Piwik](https://piwik.org/) is the leading open alternative to Google Analytics

# Volumes


# Environment Variables
## DOMAIN
The domain where the koel instance is reachable.

## PIWIK_ADMIN_NAME
- default: admin

Name of the admin user.

## PIWIK_ADMIN_PASSWORD

Password of the admin user.

## PIWIK_EMAIL

Contact email.

## PIWIK_VALID_REFERERS

Websites that are treated as vlid referers (space seperated).

## PIWIK_SITE_NAME

Title of the first website.

## PIWIK_SITE_URL

Complete url to the first website.

## PIWIK_DB_HOST
Database host.

## PIWIK_DB_NAME
Database name.

## PIWIK_DB_USER
Database user.

## PIWIK_DB_PASSWORD
Password for the database user.

## PIWIK_DB_TABLE_PREFIX
- default: piwik_

Table prefix in the database.


# Ports
- 80

# Capabilities
- CHOWN
- DAC_OVERRIDE
- FOWNER
- NET_BIND_SERVICE
- SETGID
- SETUID
